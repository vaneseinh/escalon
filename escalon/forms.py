# -*- coding: utf-8 -*-
from os.path import basename
from django.utils.translation import ugettext as _
from django import forms
from django.core.exceptions import ValidationError
from escalon.utils import error_messages

from administration.models import Contact, Category, Book, Country


class ContactForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='EMAIL')
    category = forms.ModelChoiceField(queryset=Category.objects.filter(deleted=False).all(), label="Categoría",empty_label='', required=False)
    message = forms.CharField(widget=forms.Textarea, label='MENSAJE')

    class Meta:
        model = Contact
        fields = (
            'first_name',
            'last_name',
            'email',
            'category',
            'message',
        )
        exclude = (
            'phone',
            'date_event',
        )

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = ' uk-input uk-text-input-form'

        self.fields['message'].widget.attrs['class'] += ' uk-textarea uk-text-txtarea-form'
        self.fields['category'].widget.attrs['class'] += ' uk-select '


class DinnerPicnicForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='EMAIL')
    date_event = forms.DateField(required=True, label='Fecha del evento')
    message = forms.CharField(widget=forms.Textarea, label='MENSAJE')

    class Meta:
        model = Contact
        fields = (
            'first_name',
            'last_name',
            'phone',
            'email',
            'date_event',
            'message',
        )
        exclude = (
            'category',
            'num_persons',
        )

    def __init__(self, *args, **kwargs):
        super(DinnerPicnicForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = ' uk-input uk-text-input-form'

        self.fields['message'].widget.attrs['class'] += ' uk-textarea uk-text-txtarea-form'
        self.fields['date_event'].widget.attrs['class'] += ' datepicker'


class WeddingEventForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='EMAIL')
    date_event = forms.DateField(required=True, label='Fecha del evento')
    message = forms.CharField(widget=forms.Textarea, label='MENSAJE')
    num_persons = forms.CharField(required=True,  label='Número de personas')

    class Meta:
        model = Contact
        fields = (
            'first_name',
            'last_name',
            'phone',
            'email',
            'date_event',
            'message',
            'num_persons',
        )
        exclude = (
            'category',
        )

    def __init__(self, *args, **kwargs):
        super(WeddingEventForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = ' uk-input uk-text-input-form'

        self.fields['message'].widget.attrs['class'] += ' uk-textarea uk-text-txtarea-form'
        self.fields['date_event'].widget.attrs['class'] += ' datepicker'


class LoginForm(forms.Form):
    username = forms.CharField(label=('Usuario'), max_length=128)
    password = forms.CharField(label=_(u"Contraseña"), widget=forms.PasswordInput, max_length=128)

    def clean_password(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        user = User.objects.filter(Q(username=username)).first()
        if not user:
            raise forms.ValidationError("Este usuario no existe")
        else:
            if not user.check_password(password):
                raise forms.ValidationError("Contraseña incorrecta")
        return password

    
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = 'uk-border-rounded uk-border-2 uk-input uk-form-width-large'


class BookRoomsForm(forms.Form):
    date_in = forms.DateField(required=True, label='Entrada')
    date_out = forms.DateField(required=True, label='Salida')
    guest = forms.IntegerField(required=True, label='Huéspedes')
    children = forms.IntegerField(required=True, label='Niños')

    
    def __init__(self, *args, **kwargs):
        super(BookRoomsForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = 'uk-border-rounded  uk-input uk-input-search'
            self.fields[field].widget.attrs['placeholder'] =  self.fields[field].label
        
        self.fields['date_in'].widget.attrs['class'] += ' datepicker'
        self.fields['date_out'].widget.attrs['class'] += ' datepicker'


class PersonBookForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='Correo')
    room = forms.IntegerField(required=True, label='Habitacion')

    class Meta:
        model = Book
        fields = (
            'date_in',
            'date_out',
            'guest',
            'children',
            'first_name',
            'last_name',
            'email',
            'phone',
            'requirements',
            'country',
        )


    def __init__(self, *args, **kwargs):
        super(PersonBookForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = 'uk-border-rounded uk-border-2 uk-input uk-form-width-large uk-input uk-text-input-book'

        self.fields['requirements'].widget.attrs['class'] += ' uk-textarea uk-text-txtarea-book'
        self.fields['country'].widget.attrs['class'] += ' uk-select '

        self.fields['room'].widget = forms.HiddenInput()
        self.fields['date_in'].widget = forms.HiddenInput()
        self.fields['date_out'].widget = forms.HiddenInput()
        self.fields['guest'].widget = forms.HiddenInput()
        self.fields['children'].widget = forms.HiddenInput()
