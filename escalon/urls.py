"""escalon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
#from django.views.static import serve

from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='index'),
    path('admin/', admin.site.urls),
    path('administration/', include(('administration.urls','administration'))),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('rooms', views.RoomsView.as_view(), name='rooms'),
    path('hotel', views.HotelView.as_view(), name='hotel'),
    path('contact', views.ContactView.as_view(), name='contact'),
    path('dinner', views.DinnerView.as_view(), name='dinner'),
    path('picnic', views.PicnicView.as_view(), name='picnic'),
    path('wedding', views.WeddingView.as_view(), name='wedding'),
    path('event', views.EventView.as_view(), name='event'),
    path('book_search', views.BookSearchView.as_view(), name='book_search'),
    path('ajax/bookstepone/', views.load_roombook, name='ajax_book_stepone'),
    path('booksteptow/<int:room>/<str:datein>/<str:dateout>/<int:guest>/<int:children>/', views.BookStepTwoView.as_view(), name='book_steptwo'),
    path('book/thanks', views.ThanksView.as_view(), name='thanks'),
    
    path('book_search/<int:room>/', views.BookSearchView.as_view(), name='book_search'),

]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

