# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError


from django.contrib.contenttypes.models import ContentType
from escalon import settings
from django.utils.deconstruct import deconstructible
from django.template.defaultfilters import filesizeformat
from django.core.files.storage import FileSystemStorage
from django.core.files.base import File
#from magic import from_buffer
import os
import datetime

error_messages = {
    'required': _(u"El campo es requerido."),
    'max_size': _(u"Tamaño de archivo máximo excedido %(max_size)s."
                  u" El tamaño del archivo es de %(size)s."),
    'min_size': _(u"Tamaño de archivo mínimo no alcanzado %(min_size)s."
                  u" El tamaño del archivo es de %(size)s."),
    'content_type': _(u"No soporta el tipo de archivo %(content_type)s."),
    'invalid_type': _(u"No soporta el tipo de archivo."),
    'invalid_image': _(u"El archivo no es una imagen."),
    'invalid_user': _(u"El usuario ingresado es inválido."),
    'invalid_user_exist': _(u"El usuario ingresado no esta disponible."),
    'invalid_max_files': _(u"Excede el máximo de archivos permitidos."),
}

empty_label = "Seleccione una opción"


PhoneNumberRegexValidator = RegexValidator(
    regex=r'^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$',
    message=_(u"Número telefónico inválido.")
)

@deconstructible
class DateTimeRangeValidator(object):
    """docstring for DateTimeRangeValidator"""
    error_messages = {
     'max_date': _(u"La fecha debe ser después de %(max_date)s."),
     'min_date': _(u"La fecha debe ser antes de %(min_date)s."),
    }

    def __init__(self, max_date=None, min_date=None, verbose_max_date=None, verbose_min_date=None):
        if hasattr(max_date, '__call__'):
            max_date = max_date()
        if hasattr(min_date, '__call__'):
            min_date = min_date()
        if isinstance(max_date, datetime.datetime):
            max_date = max_date.date()
        if isinstance(min_date, datetime.datetime):
            min_date = min_date.date()
        self.max_date = max_date
        self.min_date = min_date
        if verbose_max_date is not None:
            self.verbose_max_date = verbose_max_date
        else:
            self.verbose_max_date = max_date
        if verbose_min_date is not None:
            self.verbose_min_date = verbose_min_date
        else:
            self.verbose_min_date = min_date

    def __call__(self, data):
        params = {}
        error_list = []
        if self.max_date is not None and data > self.max_date:
            params = {
                'max_date': self.verbose_max_date,
            }
            error_list.append(
                ValidationError(self.error_messages['max_date'], 'max_date', params)
            )
        if self.min_date is not None and data < self.min_date:
            params = {
                'min_date': self.verbose_min_date,
            }
            error_list.append(
                ValidationError(self.error_messages['min_date'], 'min_date', params)
            )
        if error_list != []:
            print(error_list)
            raise ValidationError(error_list)

    def __eq__(self, other):
        if isinstance(other, FileValidator):
            return self.max_date == other.max_date and self.min_date == other.min_date
        else:
            return False


def validate_datetime_range(value, max_date=None, min_date=None, verbose_max_date=None, verbose_min_date=None):
    FileValidator(max_date=max_date, min_date=min_date, verbose_max_date=verbose_max_date, verbose_min_date=verbose_min_date)(value)



@deconstructible
class FileValidator(object):
    DEFAULT_CONTENT_TYPE = 'application/x-empty'

    def __init__(self, max_size=None, min_size=None, content_types=()):
        self.max_size = max_size
        self.min_size = min_size
        self.content_types = content_types

    def __call__(self, data):
        params = {}
        error_list = []
        if hasattr(data, 'size'):
            data_size = data.size
        elif hasattr(data, '__len__'):
            try:
                data_size = len(data)
            except Exception as e:
                print(e)
                data_size = 0
                pass
        else:
            data_size = 0
        if self.max_size is not None and data_size > self.max_size:
            params = {
                'max_size': filesizeformat(self.max_size),
                'size': filesizeformat(data_size),
            }
            error_list.append(
                ValidationError(self.error_messages['max_size'], 'max_size', params)
            )
        if self.min_size is not None and data_size < self.min_size:
            params = {
                'min_size': filesizeformat(self.mix_size),
                'size': filesizeformat(data_size)
            }
            error_list.append(
                ValidationError(self.error_messages['min_size'], 'min_size', params)
            )
        if self.content_types:
            try:
                content_type = self.content_type_from_data(data)
                print(content_type)
                if content_type not in self.content_types:
                    params = {'content_type': content_type}
                    error_list.append(
                        ValidationError(error_messages['content_type'], 'content_type', params)
                    )
            except:
                pass
        if error_list:
            raise ValidationError(error_list)

    @classmethod
    def content_type_from_data(cls, data):
        SIZE = 2048
        content_type = cls.DEFAULT_CONTENT_TYPE
        if isinstance(data, File):
            data.open('rb')
            #content_type = from_buffer(data.read(SIZE), mime=True)
        elif hasattr(data, 'file'):
            if hasattr(data.file, 'name'):
                print('hola')
                #content_type = from_buffer(open(data.file.name, 'rb').read(SIZE), mime=True)
        elif hasattr(data, 'name'):
            #content_type = from_buffer(open(data.name, 'rb').read(SIZE), mime=True)
            print('hola2')
        elif type(data).__name__ == 'str' or type(data).__name__ == 'unicode':
            #content_type = from_buffer(data[:SIZE], mime=True)
            print('hola3')
        return content_type

    def __eq__(self, other):
        return isinstance(other, FileValidator)


def validate_file(value, max_size=None, min_size=None, content_types=()):
    FileValidator(max_size=max_size, min_size=min_size, content_types=content_types)(value)


