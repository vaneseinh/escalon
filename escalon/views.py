from django.views.generic import View, FormView, TemplateView, CreateView
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.db.models import Q

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

import django.views.generic
from django import forms

from escalon.settings import EMAIL_PREFIX, EMAIL_DOMAIN, DEFAULT_FROM_EMAIL, EMAIL_HOST_USER
from django.core.mail import EmailMultiAlternatives
from django.template import loader

from escalon.forms import ContactForm, LoginForm, BookRoomsForm, PersonBookForm, DinnerPicnicForm, WeddingEventForm
from administration.models import Contact, Category, Room, Book, Country

from datetime import datetime
from decimal import *
from django.contrib.messages import get_messages
#DICT_PROFILE_STATE_TYPE_CHOICES = dict((x, y) for x, y in Book.STATE_TYPE_CHOICES)

def send_mail(request, tipo, type_event):
    category_name = ""

    if (tipo=='contact'):
        email_template_name = 'email_contact.html'
        
        if request.POST['category']:
            category = Category.objects.filter(id=request.POST['category']).values('name').first()
            category_name = category['name']
        
        context = {
            'domain': "%s%s" % (EMAIL_PREFIX, EMAIL_DOMAIN),  # or your domain
            'site_name': 'El Escalón Hotel',
            'protocol': 'http',
            'email':request.POST['email'],
            'first_name':request.POST['first_name'],
            'last_name':request.POST['last_name'],
            'category':category_name,
            'message':request.POST['message'],
        }
    elif(tipo=='dinner' or tipo=='picnic'):
        email_template_name = 'email_events.html'

        context = {
            'domain': "%s%s" % (EMAIL_PREFIX, EMAIL_DOMAIN),  # or your domain
            'site_name': 'El Escalón Hotel',
            'protocol': 'http',
            'email':request.POST['email'],
            'first_name':request.POST['first_name'],
            'last_name':request.POST['last_name'],
            'phone' : request.POST['phone'],
            'date_event' : request.POST['date_event'],
            'message':request.POST['message'],
            'type_event' : type_event,
        }
        subject='El Escalón Hotel - Contáctanos'

    else:
        email_template_name = 'email_events.html'

        context = {
            'domain': "%s%s" % (EMAIL_PREFIX, EMAIL_DOMAIN),  # or your domain
            'site_name': 'El Escalón Hotel',
            'protocol': 'http',
            'email':request.POST['email'],
            'first_name':request.POST['first_name'],
            'last_name':request.POST['last_name'],
            'phone' : request.POST['phone'],
            'date_event' : request.POST['date_event'],
            'num_persons' : request.POST['num_persons'],
            'message':request.POST['message'],
            'type_event' : type_event,
        }
        subject='El Escalón Hotel - '+type_event


    from_email = request.POST['email']
    to_email = [DEFAULT_FROM_EMAIL]

    #from_email = EMAIL_HOST_USER
    #to_email = [request.POST['email'] , 'estefania.heredia@cti.espol.edu.ec']

    html_content = loader.render_to_string(email_template_name, context)
    
    try:
        email_message = EmailMultiAlternatives(subject, html_content, to=to_email, from_email=from_email)   
        email_message.attach_alternative(html_content, "text/html") 
        email_message.mixed_subtype = 'related'

        email_message.send(fail_silently=False)

        return True
    except Exception as e:
        import traceback
        traceback.print_exc()
        print(e)
        return False


def send_mail_book(book, tipo):
    if(tipo=='book'):
        email_template_name = 'email_book.html'
        from_email = DEFAULT_FROM_EMAIL
        to_email = [book.email]
    else:
        email_template_name = 'email_book_admin.html'
        from_email = DEFAULT_FROM_EMAIL
        to_email = [DEFAULT_FROM_EMAIL]

    context = {
        'domain': "%s%s" % (EMAIL_PREFIX, EMAIL_DOMAIN),  # or your domain
        'site_name': 'El Escalón Hotel',
        'protocol': 'http',
        'date_in' : book.date_in,
        'date_out' : book.date_out,
        'guest' : book.guest,
        'children' : book.children ,
        'first_name' : book.first_name ,
        'last_name' : book.last_name,
        'email' : book.email,
        'phone' :  book.phone,
        'country' : book.country.name,
        'requirements' : book.requirements,
        'room_name' : book.room.name,
        'subtotal' :  round(book.subtotal,2), 
        'tax' : round(book.tax,2),
        'total' : round(book.total,2),
    }

    #from_email = DEFAULT_FROM_EMAIL
    #to_email = [book.email , 'estefania.heredia@cti.espol.edu.ec']
    
    #from_email = EMAIL_HOST_USER
    #to_email = [request.POST['email'] , 'estefania.heredia@cti.espol.edu.ec']

    html_content = loader.render_to_string(email_template_name, context)
    subject='El Escalón Hotel - Reservación'

    try:
        email_message = EmailMultiAlternatives(subject, html_content, to=to_email, from_email=from_email)   
        email_message.attach_alternative(html_content, "text/html") 
        email_message.mixed_subtype = 'related'

        email_message.send(fail_silently=False)

        return True
    except Exception as e:
        import traceback
        traceback.print_exc()
        print(e)
        return False


class HomeView(TemplateView):
    """docstring for HomeView"""
    template_name = "index.html"

    
    def form_valid(self, form):
        return super(HomeView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Home'),
            }
        return render(request, self.template_name, data)


class RoomsView(django.views.generic.ListView):
    """docstring for RoomsView"""
    model = Room
    template_name = "rooms.html"

    def get_queryset(self):
        return super(RoomsView, self).get_queryset().filter(deleted=False).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(RoomsView, self).get_context_data(**kwargs)
        context['title'] = _(u"Habitaciones")
        return context


class HotelView(TemplateView):
    """docstring for HotelView"""
    template_name = "hotel.html"

    
    def form_valid(self, form):
        return super(HotelView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'El Hotel'),
            }
        return render(request, self.template_name, data)


class ContactView(CreateView):
    """docstring for ContactView"""
    model = Contact
    template_name = "contact.html"

    def send_mail(self, tipo, type_event): send_mail(tipo, type_event)


    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Contáctanos'),
            'form': ContactForm(),
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        try:
            category = Category.objects.filter(id=request.POST.get('category')).first()

            contact = Contact()
            contact.first_name = request.POST.get('first_name')
            contact.last_name = request.POST.get('last_name')
            contact.email = request.POST.get('email')
            contact.message = request.POST.get('message')
            contact.category = category
            contact.save()

            if send_mail(request, 'contact', ''):
                messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                return redirect("index")
            else:
                messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                return self.get(request)
            
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)
            messages.error(request, _(u'Existe un problema al guardar su información.'))
            return self.get(request)


class DinnerView(CreateView):
    """docstring for DinnerView"""
    model = Contact
    template_name = "dinnerpicnic.html"


    def send_mail(self, tipo, type_event): send_mail(tipo, type_event)


    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Cenas románticas'),
            'form': DinnerPicnicForm(),
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        try:
            category = Category.objects.filter(id=1).first()

            contact = Contact()
            contact.first_name = request.POST.get('first_name')
            contact.last_name = request.POST.get('last_name')
            contact.email = request.POST.get('email')
            contact.phone = request.POST.get('phone')
            contact.date_event = request.POST.get('date_event')
            contact.message = request.POST.get('message')
            contact.category = category
            contact.save()

            if send_mail(request, 'dinner', category.name):
                messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                return redirect("index")
            else:
                messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                return self.get(request)
            
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)
            messages.error(request, _(u'Existe un problema al guardar su información.'))
            return self.get(request)
            

class PicnicView(CreateView):
    """docstring for ContactView"""
    model = Contact
    template_name = "dinnerpicnic.html"

    def send_mail(self, tipo, type_event): send_mail(tipo, type_event)


    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Picnic'),
            'form': DinnerPicnicForm(),
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        try:
            category = Category.objects.filter(id=2).first()

            contact = Contact()
            contact.first_name = request.POST.get('first_name')
            contact.last_name = request.POST.get('last_name')
            contact.email = request.POST.get('email')
            contact.phone = request.POST.get('phone')
            contact.date_event = request.POST.get('date_event')
            contact.message = request.POST.get('message')
            contact.category = category
            contact.save()

            if send_mail(request, 'picnic', category.name):
                messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                return redirect("index")
            else:
                messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                return self.get(request)
            
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)
            messages.error(request, _(u'Existe un problema al guardar su información.'))
            return self.get(request)


class WeddingView(CreateView):
    """docstring for ContactView"""
    model = Contact
    template_name = "weddingevent.html"

    def send_mail(self, tipo, type_event): send_mail(tipo, type_event)


    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Matrimonios'),
            'form': WeddingEventForm(),
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        try:
            category = Category.objects.filter(id=4).first()

            contact = Contact()
            contact.first_name = request.POST.get('first_name')
            contact.last_name = request.POST.get('last_name')
            contact.email = request.POST.get('email')
            contact.phone = request.POST.get('phone')
            contact.date_event = request.POST.get('date_event')
            contact.num_persons = request.POST.get('num_persons')
            contact.message = request.POST.get('message')
            contact.category = category
            contact.save()

            if send_mail(request, 'wedding', category.name):
                messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                return redirect("index")
            else:
                messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                return self.get(request)
            
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)
            messages.error(request, _(u'Existe un problema al guardar su información.'))
            return self.get(request)


class EventView(CreateView):
    """docstring for ContactView"""
    model = Contact
    template_name = "weddingevent.html"

    def send_mail(self, tipo, type_event): send_mail(tipo, type_event)


    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Eventos'),
            'form': WeddingEventForm(),
            }
        return render(request, self.template_name, data)

    def post(self, request, *arg, **kwargs):
        try:
            category = Category.objects.filter(id=3).first()

            contact = Contact()
            contact.first_name = request.POST.get('first_name')
            contact.last_name = request.POST.get('last_name')
            contact.email = request.POST.get('email')
            contact.phone = request.POST.get('phone')
            contact.date_event = request.POST.get('date_event')
            contact.num_persons = request.POST.get('num_persons')
            contact.message = request.POST.get('message')
            contact.category = category
            contact.save()

            if send_mail(request, 'event', category.name):
                messages.success(request, _(u'Se ha enviado correctamente su solicitud.'))
                return redirect("index")
            else:
                messages.error(request, _(u'Existe un error al enviar su solicitud.'))
                return self.get(request)
            
        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)
            messages.error(request, _(u'Existe un problema al guardar su información.'))
            return self.get(request)



class LoginView(FormView):
    form_class = LoginForm
    template_name = 'login.html'
    form = form_class()


    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Inicio de sesión'),
            'form': LoginForm(),
            }
        return render(request, self.template_name, data)

    def post(self, request, **kwargs):
        #mGCV6Ye12Ij0
        try:
            username = request.POST['username']
            password = request.POST['password']

            user = User.objects.filter(Q(username=username)).first()

            if user:
                if not user.is_active:
                    messages.error(request, _(u'La cuenta se encuentra inactiva.'))
                    return self.get(request)

                user = authenticate(username=username, password=password)
                
                if user:
                    login(request, user)
                    return redirect("administration:index")
                else:
                    messages.error(request, _(u'Contraseña inválida.'))
                    return self.get(request)
            else:
                messages.error(request, _(u'Usuario y/o contraseña inválidos.'))
                return self.get(request)

        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)


class LogoutView(LoginView):
    def get(self, request):
        logout(request)
        return super(LogoutView,self).get(request)



class BookSearchView(django.views.generic.ListView):
    model = Room
    template_name = 'book_search.html'
    paginate_by = 10
    object_list = None
    form_class = BookRoomsForm
    form = form_class()

    def post(self, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data(**kwargs)
        return render(self.request, self.template_name, context)

    def test_func(self):
        return True

    def get_queryset(self):
        if self.request.POST:

            self.form = self.form_class(self.request.POST)
            
            if self.form.is_valid():
                _datein = self.form.cleaned_data['date_in']
                _dateout = self.form.cleaned_data['date_out']
                _guest = self.form.cleaned_data['guest']
                _children = self.form.cleaned_data['children']

                if _datein and _dateout:

                    books = Book.objects.filter(deleted=False, state_type='Reserved')

                    roomsa = Room.objects.filter(deleted=False).exclude(id__in = [a.room_id for a in books])
                    #roomsa = Room.objects.exclude(id__in = [a.room_id for a in books])
                    #print(roomsa)

                    room_list = [a.id for a in roomsa]

                    for book in books:
                        if (book.verify_valid_book(_datein,_dateout)):
                            room_list.append(book.room_id)
                    return super(BookSearchView, self).get_queryset().filter(id__in = room_list).order_by('name')        
                else:
                    return super(BookSearchView, self).get_queryset().filter(deleted=False).order_by('name')
                    
        else:
            if self.kwargs.get('room'):
                return super(BookSearchView, self).get_queryset().filter(id = self.kwargs.get('room')).order_by('name')
            else:
                return super(BookSearchView, self).get_queryset().filter(deleted=False).order_by('name')
        

    def get_context_data(self, **kwargs):
        context = super(BookSearchView, self).get_context_data(**kwargs)
        context['form'] = self.form,
        context['title'] = _(u"Reserva")
        return context


def calculate_total(guest=None, days=None, roomprice=None, roomguest=None, roomguestup=None, roomcharge=None, ):
    charge = 0
    subtotal_charge = 0

    if (guest > roomguest and guest > roomguestup):
        a = guest - roomguestup
        charge = a * roomcharge

    subtotal = roomprice * days

    if (charge>0):
        subtotal_charge = subtotal + charge 
        tax = subtotal_charge * Decimal(0.12)
        total = subtotal_charge + tax
    else:
        tax = subtotal * Decimal(0.12)
        total = subtotal + tax

    return charge, subtotal_charge, subtotal, tax, total


def load_roombook(request):
    r = request.GET.get('r')
    datein = datetime.strptime(request.GET.get('di'), "%Y-%m-%d").date() 
    dateout =  datetime.strptime(request.GET.get('do'), "%Y-%m-%d").date() 
    guest = int(request.GET.get('g'))
    children = int(request.GET.get('c'))
    
    delta = dateout - datein

    room = Room.objects.filter(id=r).order_by('name').first()

    books = Book.objects.filter(room_id = room.id, deleted=False, state_type='Reserved')
    valid = True

    for book in books:
        if not(book.verify_valid_book(datein,dateout)):
            valid = False

    charge, subtotal_charge, subtotal, tax, total = calculate_total(guest, delta.days, room.week_price, room.guest, room.guest_up_type, room.charge)

    return render(request, 'book_step1.html', {'room': room, 'datein':datein, 'dateout':dateout, 'guest':guest,
        'children':children,'days':delta.days,'charge':charge,'subtotal_charge':subtotal_charge, 'subtotal':subtotal,
        'tax':tax,'total':total,'valid':valid})


class BookStepTwoView(CreateView):
    model = Book
    template_name = 'book_step2.html'

    def send_mail(self, book, tipo): send_mail_book(book, tipo)

    def form_valid(self, form):
        return super(BookStepTwoView, self).form_valid(form)

    def get(self, request, room=None, datein=None, dateout=None, guest=None, children=None, *args, **kwargs):
        room = Room.objects.filter(id=room).first()
        valid = True

        if room == None:
            valid = False
            messages.error(request,  _(u'Debe escoger una habitación para reservar.'))

        try:
            datetime.strptime(datein, '%Y-%m-%d')
        except ValueError:
            valid = False
            messages.error(request, _(u'Debe escoger una fecha de entrada para reservar.'))
        
        try:
            datetime.strptime(dateout, '%Y-%m-%d')
        except ValueError:
            valid = False
            messages.error(request, _(u'Debe escoger una fecha de salida para reservar.'))

        try:
            int(guest)
        except ValueError:
            valid = False
            messages.error(request,  _(u'Debe especificar el numero de huéspedes para reservar.'))

        try:
            int(children)
        except ValueError:
            valid = False
            messages.error(request, _(u'Debe especificar el numero de niños para reservar.'))

        if valid:
            
            datein = datetime.strptime(datein, "%Y-%m-%d").date() 
            dateout =  datetime.strptime(dateout, "%Y-%m-%d").date() 
            guest = int(guest)
            children = int(children)

            delta = dateout - datein

            charge, subtotal_charge, subtotal, tax, total = calculate_total(guest, delta.days, room.week_price, room.guest, room.guest_up_type, room.charge)

            data = {
                'title': _(u'Inicio de sesión'),
                'form': PersonBookForm(initial={ 'room':room.id, 'date_in':datein, 
                    'date_out':dateout, 'guest':guest, 'children':children,}),
                'room':room,
                'days': delta.days,
                'date_in':datein, 
                'date_out':dateout, 
                'guest':guest, 
                'children':children,
                'charge':charge,  
                'subtotal_charge':subtotal_charge,
                'subtotal':subtotal,  
                'tax':tax, 
                'total':total
            }
        else:
            data = {
                'title': _(u'Inicio de sesión'),
                'form': PersonBookForm(),
            }
        

        return render(request, self.template_name, data)


    def post(self, request,  room=None, datein=None, dateout=None, guest=None, children=None, *arg, **kwargs):
        room_id = request.POST.get('room')
        din = request.POST.get('date_in')
        dout =  request.POST.get('date_out') 
        guest = int(request.POST.get('guest'))
        children = int(request.POST.get('children'))

        datein = datetime.strptime(din, "%Y-%m-%d").date() 
        dateout =  datetime.strptime(dout, "%Y-%m-%d").date() 

        try:

            room = Room.objects.filter(id=room_id).first()
            country = Country.objects.filter(id=request.POST.get('country')).first()

            delta = dateout - datein

            valid = True

            if room == None:
                valid = False

            try:
                datetime.strptime(din, '%Y-%m-%d')
            except ValueError:
                valid = False
            
            try:
                datetime.strptime(dout, '%Y-%m-%d')
            except ValueError:
                valid = False

            try:
                int(guest)
            except ValueError:
                valid = False

            try:
                int(children)
            except ValueError:
                valid = False

            if valid:

                charge, subtotal_charge, subtotal, tax, total = calculate_total(guest, delta.days, room.week_price, room.guest, room.guest_up_type, room.charge)

                book = Book()
                book.date_in = datein
                book.date_out = dateout
                book.guest = guest
                book.children = children
                book.first_name = request.POST.get('first_name')
                book.last_name = request.POST.get('last_name')
                book.email = request.POST.get('email')
                book.phone = request.POST.get('phone')
                book.requirements = request.POST.get('requirements')
                book.subtotal = round(subtotal_charge,2) if (charge>0) else round(subtotal,2)
                book.tax = tax
                book.total = total
                book.state_type = 'Reserved'

                book.room = room
                book.country = country
                book.save()

                #if (self.send_mail(book,'book'))

                self.send_mail(book,'book')

                self.send_mail(book,'admin')

                return redirect("thanks")

                #if send_mail_book(book):
#                    messages.success(request, _(u'Se ha hecho una reservación correctamente, verifique su correo con todos los detalles.'))
#                    return redirect("thanks")
#                else:
#                    messages.error(request, _(u'Existe un error al realizar su reservación.'))
#                    return self.get(request)

                
            else:
                return self.get(request, room_id, datein, dateout, guest, children)
            
         

        except Exception as e:
            import traceback;
            traceback.print_exc()
            print(e)


class ThanksView(TemplateView):
    """docstring for HomeView"""
    template_name = "thanks.html"

    
    def form_valid(self, form):
        return super(ThanksView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Gracias'),
            }
        return render(request, self.template_name, data)