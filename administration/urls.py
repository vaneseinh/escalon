"""escalon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.conf import settings
from . import views

#mGCV6Ye12Ij0

urlpatterns = [
    path('', views.AdministrationView.as_view(), name='index'),

    path('rooms', views.RoomsListView.as_view(), name='rooms'),
    path('rooms/new', views.RoomCreateView.as_view(), name='room-new'),
    path('rooms/edit/<int:id>', views.RoomUpdateView.as_view(), name='room-edit'),
    path('rooms/delete/<int:id>', views.RoomDeleteView.as_view(), name='room-delete'),

    path('books', views.BooksListView.as_view(), name='books'),
    path('book/new', views.BookCreateView.as_view(), name='book-new'),
    path('book/edit/<int:id>', views.BookUpdateView.as_view(), name='book-edit'),
    path('book/delete/<int:id>', views.BookDeleteView.as_view(), name='book-delete'),

    path('ajax/roomcheckdate/', views.load_room_checkdate, name='ajax_room_checkdate'),
    path('ajax/calculate/', views.load_calculate, name='ajax_calculate'),
]
