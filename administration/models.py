from django.db import models
from django.utils.translation import ugettext as _
from escalon.utils import FileValidator, PhoneNumberRegexValidator, DateTimeRangeValidator
from datetime import timedelta, date
#from multiselectfield import MultiSelectField

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='Categoría')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(Category, self).delete()


class Contact(models.Model):
    first_name = models.CharField(max_length=100, verbose_name='NOMBRE')
    last_name = models.CharField(max_length=100, verbose_name='APELLIDO')
    email = models.CharField(max_length=100, verbose_name='EMAIL')
    phone = models.CharField(max_length=16, blank=True, null=True, validators=[PhoneNumberRegexValidator], verbose_name='Teléfono')
    num_persons = models.CharField(max_length=100, blank=True, null=True,  verbose_name='Número de personas')
    date_event = models.DateField(blank=True, null=True, validators=[DateTimeRangeValidator(verbose_max_date=u'Fecha del evento')], verbose_name='Fecha del evento')
    message = models.TextField(blank=True, null=True, verbose_name=u'MENSAJE')
    category = models.ForeignKey(Category, null=True,  on_delete=models.DO_NOTHING, verbose_name='Categoría')
    date_creation = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(Contact, self).delete()


class Room(models.Model):
    IMAGE_MIMETYPES = (
        'image/jpeg',
        'image/pjpeg',
        'image/png',
    )


    RATE_TYPE_CHOICES = (
        ('HAB', _(u'Por habitación')),
        ('PER', _(u'Por persona')),
    )

    GUEST_UP_TYPE_CHOICES = (
        (1, _(u'1 Adulto')),
        (2, _(u'2 Adultos')),
        (3, _(u'3 Adultos')),
    )


    name = models.CharField(max_length=100, verbose_name='NOMBRE')
    quantity = models.PositiveSmallIntegerField(verbose_name='CANTIDAD')
    available = models.PositiveSmallIntegerField(default=0, verbose_name='Disponible')
    guest = models.PositiveSmallIntegerField(verbose_name='Huéspedes')
    children = models.PositiveSmallIntegerField( default=0, verbose_name='Niños')
    size = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u'Tamaño')
    description = models.TextField(verbose_name=u'Descripción', null=False, blank=False)
    rate_type = models.CharField( max_length=3, choices=RATE_TYPE_CHOICES, default='HAB', verbose_name=u'Tipo de tarifa')
    week_price = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u'Precio entre semana')
    weekend_price = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u'Precio entre fin de semana')
    weekly_price = models.DecimalField(blank=True, null=True, max_digits=5, decimal_places=2, verbose_name=u'Precio semanal')
    monthly_price = models.DecimalField(blank=True, null=True, max_digits=5, decimal_places=2, verbose_name=u'Precio mensual')
    guest_up_type = models.PositiveSmallIntegerField(blank=True, null=True, choices=GUEST_UP_TYPE_CHOICES, verbose_name=u'Por cada huésped arriba de')
    charge = models.DecimalField(blank=True, null=True, max_digits=5, decimal_places=2, verbose_name=u'Cargo')
    
    service_types = models.ManyToManyField( 'ServicesRoom', verbose_name=u'Tipo de servicio')

    attach_files = models.ManyToManyField('AttachFile', blank=True, verbose_name='Imágenes')
    
    deleted = models.BooleanField(default=False)
    create_date = models.DateTimeField(auto_now_add=True, null=True)
    update_date = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(Room, self).delete()


class ServicesRoom(models.Model):
    service_type = models.CharField(max_length=100, verbose_name=u'Tipo de servicio')
    
    def __str__(self):
        return self.service_type

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(ServicesRoom, self).delete()


class BedRoom(models.Model):
    BED_TYPE_CHOICES = (
        ('IND', _(u'Individual')),
        ('DOB', _(u'Doble')),
        ('QUE', _(u'Queen')),
        ('KIN', _(u'King')),
        ('SKIN', _(u'Súper King')),
    )

    bed_type = models.CharField( max_length=4, choices=BED_TYPE_CHOICES, default='IND', verbose_name=u'Tipo de cama')
    quantity = models.PositiveSmallIntegerField(default=1, verbose_name='CANTIDAD')
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        return self.bed_type

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(BedRoom, self).delete()


class AttachFile(models.Model):
    """
    docstring for AttachFile
    """
    file_name = models.CharField(max_length=100, blank=True)

    attach_file = models.FileField(
        upload_to='documents/room/%Y/%m/%d',
        validators=[FileValidator,],
        verbose_name=u'Adjuntar imagen'
    )

    attach_file_src = ''

    attach_file_size = models.PositiveIntegerField(default=0)

    content_type = models.CharField(max_length=128, blank=True)

    def __unicode__(self):
        return self.file_name

    def get_attach_file_src(self):
        return self.attach_file

    def delete(self, *args, **kwargs):
        if self.attach_file:
            self.attach_file.delete(save=False)
        super(AttachFile, self).delete()

    def save(self, *args, **kwargs):
        self.attach_file_size = self.attach_file.size
        if hasattr(self, "content_type"):
            self.content_type = FileValidator.content_type_from_data(self.attach_file)
        try:
            this = AttachFile.objects.get(id=self.id)
            if this.attach_file != self.attach_file:
                this.attach_file.delete(save=False)
        except Exception as e: print("Exception", e); pass # when new photo then we do nothing, normal case
        super(AttachFile, self).save(*args, **kwargs)


class Country(models.Model):
    name = models.CharField(max_length=30, verbose_name='País')
    code = models.CharField(max_length=3, null=True, unique=True, verbose_name='Código')

    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def delete(self):
        self.deleted = True
        self.save()

    def hard_delete(self):
        super(Country, self).delete()


class Book(models.Model):
    STATE_TYPE_CHOICES = (
        ('Reserved', _(u'Reservado')),
        ('CheckedIn', _(u'Check-in finalizado')),
        ('CheckedOut', _(u'Check-out finalizado')),
        ('NotAvailable', _(u'No disponible')),
        ('Cancelled', _(u'Cancelado')),
    )

    date_in = models.DateField(validators=[DateTimeRangeValidator(verbose_max_date=u'Entrada')], verbose_name='Entrada')
    date_out = models.DateField(validators=[DateTimeRangeValidator(verbose_max_date=u'Salida')], verbose_name='Salida' )
    guest = models.PositiveSmallIntegerField(verbose_name='Huéspedes')
    children = models.PositiveSmallIntegerField( verbose_name='Niños')

    room = models.ForeignKey(Room, on_delete=models.DO_NOTHING)

    first_name = models.CharField(max_length=100, verbose_name='NOMBRE')
    last_name = models.CharField(max_length=100, verbose_name='APELLIDO')
    email = models.CharField(max_length=100, verbose_name='EMAIL')
    phone = models.CharField( max_length=16, validators=[PhoneNumberRegexValidator], verbose_name='Teléfono')
    requirements = models.TextField(blank=True, null=True, verbose_name=u'Requerimientos especiales')

    country = models.ForeignKey(Country, on_delete=models.DO_NOTHING, verbose_name=u'País')

    subtotal = models.DecimalField(default=0, max_digits=5, decimal_places=2, verbose_name=u'Subtotal')
    tax = models.DecimalField(default=0, max_digits=5, decimal_places=2, verbose_name=u'Impuestos (12%)')
    total = models.DecimalField(default=0, max_digits=5, decimal_places=2, verbose_name=u'Precio semanal')

    state_type = models.CharField( max_length=20, choices=STATE_TYPE_CHOICES, verbose_name=u'Estado de la reserva')
    
    deleted = models.BooleanField(default=False)
    create_date = models.DateTimeField(auto_now_add=True, null=True)
    update_date = models.DateTimeField(auto_now=True, null=True)

    def delete(self):
        self.deleted = True
        self.save()


    def hard_delete(self):
        super(Room, self).delete()

    @property
    def book_by(self):
        return "%s %s" % (self.first_name, self.last_name)

    @property
    def book_state(self):
        for key,value in self.STATE_TYPE_CHOICES:
            if(key==self.state_type):
                return value


    def daterange(self,date1, date2):
        for n in range(int ((date2 - date1).days)+1):
            yield date1 + timedelta(n)


    def verify_valid_book(self, start_dt, end_dt):
        valid = True

        for dt in self.daterange(start_dt, end_dt):
            a = dt.strftime("%Y-%m-%d")
            
            for dt2 in self.daterange(self.date_in, self.date_out):
              b = dt2.strftime("%Y-%m-%d")
              
              if a==b:
                valid=False

        return valid
        

        