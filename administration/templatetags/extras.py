from django import template
from django.conf import settings
from django.contrib.auth.models import Group, User
from datetime import datetime

register = template.Library()


@register.filter
def remove_minus(value):
    return value.replace("-", "_")


@register.filter
def join_by(value, arg):
    return arg.join(value)


def paginator(context):
    adjacent_pages = 3
    _paginator = context.get('paginator', None)
    _page_obj = context.get('page_obj', None)
    _page = _page_obj.number
    startPage = max(_page - adjacent_pages + 1, 1)
    _pages = _paginator.num_pages

    endPage = min(startPage + adjacent_pages, _pages + 1)

    page_numbers = [n for n in range(startPage, endPage) \
                    if n > 0 and n <= _pages]
    page_obj = _page_obj
    paginator = _paginator

    if (_pages - 1) == _page and _pages >= 4:
        page_numbers.append(_pages)
    if (1 not in page_numbers and 2 in page_numbers):
        page_numbers = [1] + page_numbers

    return {
        'page_obj': page_obj,
        'paginator': paginator,
        'page': _page_obj,
        'pages': _pages,
        'page_numbers': page_numbers,
        'show_first': 1 not in page_numbers and 2 not in page_numbers,
        'show_last': (_pages - 1) not in page_numbers,
    }


register.inclusion_tag('paginator.html', takes_context=True)(paginator)


# settings value
@register.simple_tag
def settings_var(name):
    return getattr(settings, name, "")



@register.filter(name='days')
def days(value, arg):
    do = value
    di = arg

    delta = do - di

    return delta.days



