# -*- coding: utf-8 -*-
from os.path import basename
from django.utils.translation import ugettext as _
from django import forms
from django.core.exceptions import ValidationError
from escalon.utils import error_messages

from administration.widgets import  ClearableFileInputMultiple, SelectMultipleChoice
from escalon.settings import MAX_FILES_MULTIPLE_FILES, MAX_UPLOAD_SIZE

from administration.models import Room, BedRoom, Book, Country, AttachFile, ServicesRoom
from django.forms import inlineformset_factory
from django.forms import formset_factory

class RoomForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea, label='MENSAJE')

    service_types = forms.ModelMultipleChoiceField(label='Tipo de servicio', widget=SelectMultipleChoice(), queryset=ServicesRoom.objects.all().order_by('id'), required=True)

    attach_files = forms.FileField(
        widget=ClearableFileInputMultiple(attrs={'multiple': True}),
        required=True,
        label='Imágenes'
    )

    class Meta:
        model = Room
        fields = (
            'name',
            'quantity',
            'guest',
            'children',
            'size',
            'description',
            'rate_type',
            'week_price',
            'weekend_price',
            'weekly_price',
            'monthly_price',
            'guest_up_type',
            'charge',
            'attach_files',
            'service_types',
        )

    def clean_attach_files(self):
        attach_files = self.cleaned_data.get('attach_files')
        if attach_files is None: attach_files = []
        if len(self.file_list) > MAX_FILES_MULTIPLE_FILES:
            raise forms.ValidationError(error_messages['invalid_max_files'])

        return attach_files    

    def __init__(self, *args, **kwargs):
        self.file_list = kwargs.pop('file_list', [])
        self.is_update = kwargs.pop('is_update', False)
        
        super(RoomForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = 'uk-border-rounded uk-border-2 uk-input'

        self.fields['rate_type'].widget.attrs['class'] += ' uk-select'
        self.fields['guest_up_type'].widget.attrs['class'] += ' uk-select'
        self.fields['attach_files'].widget.attrs['class'] += ' uk-input'
        self.fields['service_types'].widget.attrs['class'] = 'uk-checkbox'
        

        if self.is_update == True:

            self.fields['service_types'].initial = self.instance.service_types.values_list('id',flat=True)

            if hasattr(self.instance, 'attach_files'):
                self.fields['attach_files'].widget.file_list = self.instance.attach_files.all()


    def save(self, *args, **kwargs):
        obj = super(RoomForm, self).save(commit=False,*args, **kwargs)
        obj.save()

        sevices_selected = self.cleaned_data.get('service_types')
        
        if sevices_selected:
            for service in obj.service_types.all():
                obj.service_types.remove(service)

            for service in sevices_selected:
                obj.service_types.add(service)

        p = self.cleaned_data.get('attach_files')
        #print(p)
        #print(self.file_list)
        #if p and self.file_list:
        #    if type(self.cleaned_data.get('attach_files')).__name__ != 'QuerySet' and 'attach_files' in self.fields:
        #        for _attach_file in obj.attach_files.all():
        #            _attach_file.delete()#

        if self.file_list:
            for _file_ in self.file_list:
                attach_file = AttachFile(
                    file_name=_file_.name[0:100],
                    attach_file=_file_
                )
                attach_file.save()
                obj.attach_files.add(attach_file)#
        return obj


class BedRoomForm(forms.ModelForm): 
    class Meta:
        model = BedRoom
        fields = (
            'bed_type',
            'quantity',
        )

        exclude = ('room', 'id')
    

    def __init__(self, *args, **kwargs):
        super(BedRoomForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = 'uk-border-rounded uk-border-2 uk-input'
            self.fields[field].required = False

        self.fields['bed_type'].widget.attrs['class'] += ' uk-select'


    def save(self, *args, **kwargs):
        return super(BedRoomForm, self).save(*args, **kwargs)


BedRoomFormSet = formset_factory(BedRoomForm, extra=0,  min_num=1, can_delete=False)



class BookForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='Correo') 
    room = forms.ModelChoiceField(queryset=Room.objects.filter(deleted=False).order_by('name').all(), label="Habitación", empty_label='Elige una habitación', required=True)

    class Meta:
        model = Book
        fields = (
            'date_in',
            'date_out',
            'guest',
            'children',
            'first_name',
            'last_name',
            'email',
            'phone',
            'requirements',
            'country',
            'state_type',
            'room'
        )

    def __init__(self, *args, **kwargs):
        self.is_update = kwargs.pop('is_update', False)
        super(BookForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].error_messages = error_messages
            self.fields[field].widget.attrs['class'] = 'uk-border-rounded uk-border-2 uk-input'

        self.fields['requirements'].widget.attrs['class'] += ' uk-textarea uk-text-txtarea-book'
        self.fields['country'].widget.attrs['class'] += ' uk-select '
        self.fields['room'].widget.attrs['class'] += ' uk-select '

        self.fields['date_in'].widget.attrs['class'] += ' datepicker'
        self.fields['date_out'].widget.attrs['class'] += ' datepicker'


    def save(self, *args, **kwargs):
        return super(BookForm, self).save(*args, **kwargs)

