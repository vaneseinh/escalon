# Generated by Django 2.0.7 on 2018-08-05 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0007_auto_20180804_1907'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='code',
            field=models.CharField(max_length=3, null=True, unique=True, verbose_name='Código'),
        ),
        migrations.AlterField(
            model_name='room',
            name='guest_up_type',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(1, '1 Adulto'), (2, '2 Adultos'), (3, '3 Adultos')], max_length=2, null=True, verbose_name='Por cada huésped arriba de'),
        ),
    ]
