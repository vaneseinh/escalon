from administration.models import Room
from escalon.settings import MAX_UPLOAD_SIZE

from django.forms.widgets import ClearableFileInput, CheckboxSelectMultiple
from django.template import loader

from mimetypes import MimeTypes
import base64




class ClearableFileInputMultiple(ClearableFileInput):
    """docstring for ClearableFileInputMultiple"""
    template_name = 'input_media_multiple.html'

    def __init__(self, *args, **kwargs):
        self.file_list = kwargs.pop('file_list', [])
        super(ClearableFileInputMultiple, self).__init__(*args, **kwargs)

    def get_context(self, name, value, attrs):
        context = super(ClearableFileInputMultiple, self).get_context(name, value, attrs)
        context['file_list'] = self.file_list
        context['valid_mimetypes_list'] = ','.join(Room.IMAGE_MIMETYPES)
        return context


class SelectMultipleChoice(CheckboxSelectMultiple):
    """docstring for ClearableFileInputMultiple"""
    template_name = 'input_multiple_select.html'

    def __init__(self, *args, **kwargs):
        super(SelectMultipleChoice, self).__init__(*args, **kwargs)

    #def get_context(self, name, value, attrs):
    #    context = super(ClearableFileInputMultiple, self).get_context(name, value, attrs)
    #    context['file_list'] = self.file_list
    #    context['valid_mimetypes_list'] = ','.join(Room.IMAGE_MIMETYPES)
    #    return context