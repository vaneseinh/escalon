from django.contrib import admin

# Register your models here.
from administration.models import Room

admin.site.register(Room)
