from django.shortcuts import render
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect
from django.db.models import Q
from django.views.generic.base import View
from django.contrib import messages

import django.views.generic
from django.urls import reverse_lazy
from django.forms import formset_factory

from django.contrib.auth.mixins import UserPassesTestMixin, PermissionRequiredMixin

from django.core.paginator import Paginator

from administration.models import Room, BedRoom,  Book, Country
from administration.forms import RoomForm, BedRoomForm, BookForm

from administration.forms import  BedRoomFormSet

from datetime import datetime
from decimal import *

from django.db import transaction


import os
from datetime import timedelta
import datetime
import pytz
from datetime import datetime, date,time


#import httplib2
#from googleapiclient.discovery import build
#from oauth2client.service_account import ServiceAccountCredentials



# Create your views here.
#mGCV6Ye12Ij0
class AdministrationView(UserPassesTestMixin, View):
    template_name = "admin_index.html"

    def test_func(self):
        _user_ = self.request.user
        return _user_.is_superuser

    def form_valid(self, form):
        return super(AdministrationView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        data = {
            'title': _(u'Administración'),
            }
        return render(request, self.template_name, data)


class RoomsListView(UserPassesTestMixin, django.views.generic.ListView):
    template_name = "rooms_list.html"
    model = Room
    paginate_by = 10

    def test_func(self):
        _user_ = self.request.user
        return _user_.is_superuser

    def get_queryset(self):
        return super(RoomsListView, self).get_queryset().filter(deleted=False).order_by('id')

    def get_context_data(self, **kwargs):
        context = super(RoomsListView, self).get_context_data(**kwargs)
        context['title'] = _(u"Tipos de habitaciones")
        context['show_tb_header'] = False
        return context



class RoomCreateView( UserPassesTestMixin, django.views.generic.CreateView):
    """docstring for BannerUpdateView"""
    model = Room
    template_name = 'room_edit.html'
    form_class = RoomForm

    def handle_no_permission(self):
        raise Http404

    def test_func(self):
        _user_ = self.request.user
        if _user_.is_superuser:
            return True
        return False

    def get_success_url(self):
        return reverse_lazy('administration:rooms')

    def get_form_kwargs(self):
        kwargs = super(RoomCreateView, self).get_form_kwargs()
        kwargs['file_list'] = self.request.FILES.getlist('attach_files')
        kwargs['is_update'] = False
        return kwargs

    def get_object(self):
        qs = self.model.objects.filter(pk=self.kwargs['id'])
        if not qs.exists():
            raise Http404
        _object = qs.first()
        return _object

    def form_valid(self, form):
        context = self.get_context_data()
        beds = context['formset_2']#

        #print(beds)
       

        with transaction.atomic():

            room = form.save()

            for bed in BedRoom.objects.filter(room=room):
                bed.hard_delete()

            for bed in beds:
                #print('bed')
                #print(bed.instance.bed_type)
                #print(bed.instance.quantity)
                if bed.is_valid():
                    form = bed.save(commit=False)
                    form.room = room
                    form.save()
                else:
                    messages.error(self.request, _(u'Si desea agregar mas camas '))
                    return render(self.request, self.template_name, context)


        #response = super(AppointmentUpdateView, self).form_valid(form)
        return super(RoomCreateView, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(RoomCreateView, self).get_context_data(**kwargs)
        context['title'] = _(u"Habitación")
        context['isUpdate'] = False

        if self.request.POST:
            context['formset_2'] = BedRoomFormSet(self.request.POST)
        else:
            context['formset_2'] = BedRoomFormSet()

        return context



class RoomUpdateView( UserPassesTestMixin, django.views.generic.UpdateView):
    """docstring for BannerUpdateView"""
    model = Room
    template_name = 'room_edit.html'
    form_class = RoomForm

    def handle_no_permission(self):
        raise Http404

    def test_func(self):
        _user_ = self.request.user
        if _user_.is_superuser:
            return True
        return False

    def get_success_url(self):
        return reverse_lazy('administration:rooms')

    def get_form_kwargs(self):
        kwargs = super(RoomUpdateView, self).get_form_kwargs()
        kwargs['file_list'] = self.request.FILES.getlist('attach_files')
        kwargs['is_update'] = True
        return kwargs

    def get_object(self):
        qs = self.model.objects.filter(pk=self.kwargs['id'])
        if not qs.exists():
            raise Http404
        _object = qs.first()
        return _object

    def form_valid(self, form):
        context = self.get_context_data()
        beds = context['formset_2']

        with transaction.atomic():

            room = form.save()

            for bed in BedRoom.objects.filter(room=room):
                bed.hard_delete()

            for bed in beds:
                if bed.is_valid():
                    name = bed.cleaned_data.get('bed_type')
                    form = bed.save(commit=False)
                    form.room = room
                    form.save()
                else:
                    messages.error(self.request, _(u'Si desea agregar mas camas '))
                    return render(self.request, self.template_name, context)

        return super(RoomUpdateView, self).form_valid(form)



    def get_context_data(self, **kwargs):
        context = super(RoomUpdateView, self).get_context_data(**kwargs)
        context['title'] = _(u"Habitación")
        context['isUpdate'] = True

        room = self.get_object()

        if self.request.POST:
            context['formset_2'] = BedRoomFormSet(self.request.POST)
        else:
            context['formset_2'] = BedRoomFormSet(initial=BedRoom.objects.filter(room=room).values())
    
        return context



class RoomDeleteView(UserPassesTestMixin, django.views.generic.DeleteView):
    model = Room
    success_url = reverse_lazy('administration:rooms')

    def test_func(self):
        _user_ = self.request.user
        return _user_.is_superuser

    def get_object(self):
        qs = self.model.objects.filter(id=self.kwargs['id'], deleted=False)
        if qs.exists():
            return qs.first()
        raise Http404

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class BooksListView(UserPassesTestMixin, django.views.generic.ListView):
    template_name = "book_list.html"
    model = Book
    paginate_by = 10

    def test_func(self):
        _user_ = self.request.user
        return _user_.is_superuser

    def get_queryset(self):
        return super(BooksListView, self).get_queryset().filter(deleted=False).order_by('date_in')


    def create_event(self):
        bookings = Book.objects.filter(deleted=False, state_type='Reserved').order_by('date_in')
        events = []
        timea = time(13, 00)

        for event in bookings:
            s = datetime.combine(event.date_in, timea)
            start = s.isoformat()

            e = datetime.combine(event.date_out, timea)
            end = e.isoformat()
           
            room = event.room.name

            events.append({ 'title':  room, 'start': start, 'end': end, 'id': event.id})

        return events

    def get_context_data(self, **kwargs):
        context = super(BooksListView, self).get_context_data(**kwargs)
        context['title'] = _(u"Revervaciones")
        context['show_tb_header'] = True
        context['events'] = self.create_event()   
        return context



def load_room_checkdate(request):
    di = request.GET.get('di')
    do = request.GET.get('do')

    books = Book.objects.filter(deleted=False, state_type=DICT_PROFILE_STATE_TYPE_CHOICES['Reserved'])
    roomsa = Room.objects.exclude(id__in = [a.room_id for a in books])
    room_list = [a.id for a in roomsa]

    
    if di and do:
        datein = datetime.strptime(request.GET.get('di'), "%Y-%m-%d").date() 
        dateout =  datetime.strptime(request.GET.get('do'), "%Y-%m-%d").date() 
        
        for book in books:
            if (book.verify_valid_book(_datein,_dateout)):
                room_list.append(book.room_id)

        room_list = Room.objects.filter(id__in = [a.id for a in p])
    else:
        room_list = Room.objects.filter(deleted=False).order_by('name').all()

    return render(request, 'ajax_room_available.html', {'object_list': room_list})


def calculate_total(guest=None, days=None, roomprice=None, roomguest=None, roomguestup=None, roomcharge=None, ):
    charge = 0
    subtotal_charge = 0

    if (guest > roomguest):
        if (roomguestup and guest > roomguestup):
            a = guest - roomguestup
            charge = a * roomcharge

    subtotal = roomprice * days

    if (charge>0):
        subtotal_charge = subtotal + charge 
        tax = subtotal_charge * Decimal(0.12)
        total = subtotal_charge + tax
    else:
        tax = subtotal * Decimal(0.12)
        total = subtotal + tax

    return charge, subtotal_charge, subtotal, tax, total


def load_calculate(request):
    r = request.GET.get('r')
    datein = datetime.strptime(request.GET.get('di'), "%Y-%m-%d").date() 
    dateout =  datetime.strptime(request.GET.get('do'), "%Y-%m-%d").date() 
    guest = int(request.GET.get('g'))
    children = int(request.GET.get('c'))
    
    delta = dateout - datein

    room_list = Room.objects.filter(id=r).order_by('name')
    room = room_list[0]

    charge, subtotal_charge, subtotal, tax, total = calculate_total(guest, delta.days, room.week_price, room.guest, room.guest_up_type, room.charge)

    return render(request, 'ajax_calculate.html', {'room': room, 'datein':datein, 'dateout':dateout, 'guest':guest,
        'children':children,'days':delta.days,'charge':charge,'subtotal_charge':subtotal_charge, 'subtotal':subtotal,'tax':tax,'total':total,})



class BookCreateView(UserPassesTestMixin, django.views.generic.CreateView):
    """docstring for CountryUpdateView"""
    model = Book
    form_class = BookForm
    template_name = 'book_edit.html'
    success_url = reverse_lazy('administration:books')

    def test_func(self):
        _user_ = self.request.user
        return _user_.is_superuser

    def form_valid(self, form):
        return super(BookCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BookCreateView, self).get_context_data(**kwargs)
        context['title'] = "Crear una nueva reserva"
        context['update'] = False
        return context



    def post(self, request,  *arg, **kwargs):
        book_form = BookForm(request.POST)

        if book_form.is_valid():

            room_id = request.POST.get('room')
            room = Room.objects.filter(id=room_id).first()

            book = book_form.save()

            din = request.POST.get('date_in')
            dout =  request.POST.get('date_out') 
            guest = int(request.POST.get('guest'))
            children = int(request.POST.get('children'))

            datein = datetime.strptime(din, "%Y-%m-%d").date() 
            dateout =  datetime.strptime(dout, "%Y-%m-%d").date() 
            delta = dateout - datein

            charge, subtotal_charge, subtotal, tax, total = calculate_total(guest, delta.days, room.week_price, room.guest, room.guest_up_type, room.charge)

            book.subtotal = round(subtotal_charge,2) if (charge>0) else round(subtotal,2)
            book.tax = tax
            book.total = total
            book.save()

            return redirect("administration:books")
        else:
            return self.get(request)


class BookUpdateView(UserPassesTestMixin, django.views.generic.UpdateView):
    """docstring for CountryUpdateView"""
    model = Book
    form_class = BookForm
    template_name = 'book_edit.html'
    success_url = reverse_lazy('administration:books')

    def test_func(self):
        _user_ = self.request.user
        return _user_.is_superuser

    def form_valid(self, form):
        try:
            response = super(BookUpdateView, self).form_valid(form)
        except Exception as e:
            response = super(BookUpdateView, self).form_invalid(form)
        return response

    def get_object(self):
        qs = self.model.objects.filter(id=self.kwargs['id'], deleted=False)
        print(qs)
        if qs.exists():
            return qs.first()
        raise Http404

    def get_context_data(self, **kwargs):
        context = super(BookUpdateView, self).get_context_data(**kwargs)
        context['title'] = "Modificar la reserva"
        context['update'] = True
        return context

    def post(self, request,  *arg, **kwargs):
        book_form = BookForm(request.POST)

        if book_form.is_valid():

            roomid_old = request.POST.get('room_old')
            roomid_new = request.POST.get('room_new')

            #print(book_form)

            book = book_form.save()

            #print(book)

            room_id = request.POST.get('room')  
            room = Room.objects.filter(id=room_id).first()

            #print(room)

            room_id = request.POST.get('room')
            din = request.POST.get('date_in')
            dout =  request.POST.get('date_out') 
            guest = int(request.POST.get('guest'))
            children = int(request.POST.get('children'))

            datein = datetime.strptime(din, "%Y-%m-%d").date() 
            dateout =  datetime.strptime(dout, "%Y-%m-%d").date() 
            delta = dateout - datein

            charge, subtotal_charge, subtotal, tax, total = calculate_total(guest, delta.days, room.week_price, room.guest, room.guest_up_type, room.charge)

            book.subtotal = round(subtotal_charge,2) if (charge>0) else round(subtotal,2)
            book.tax = tax
            book.total = total
            book.save()

            return redirect("administration:books")

        else:
            return self.get(request)
        


class BookDeleteView(UserPassesTestMixin, django.views.generic.DeleteView):
    model = Book
    success_url = reverse_lazy('administration:books')

    def test_func(self):
        _user_ = self.request.user
        return _user_.is_superuser

    def get_object(self):
        qs = self.model.objects.filter(id=self.kwargs['id'], deleted=False)
        if qs.exists():
            return qs.first()
        raise Http404

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)





#    service_account_email = 'escalon@escalon-216604.iam.gserviceaccount.com'#

#    CLIENT_SECRET_FILE = 'escalon-216604-82e66a0d5113.p12'#

#    SCOPES = 'https://www.googleapis.com/auth/calendar'
#    scopes = [SCOPES]#

#    def build_service(self):
#        credentials = ServiceAccountCredentials.from_p12_keyfile(
#            service_account_email=self.service_account_email,
#            filename=self.CLIENT_SECRET_FILE,
#            scopes=self.SCOPES
#        )#

#        http = credentials.authorize(httplib2.Http())#

#        service = build('calendar', 'v3', http=http)#

#        return service

#    def create_event(self):
#        service = self.build_service()#

#        start_datetime = datetime.now(tz=pytz.utc)#

#        print(start_datetime)
#        print(start_datetime.isoformat())

        #datetime_object = datetime.strptime(war_start, '%Y-%m-%dT%H:%M:%S.%f')
        #datetime_object2 = datetime.strptime(war_start2, '%Y-%m-%dT%H:%M:%S.%f')
#

#        tz = pytz.timezone("Australia/Melbourne")
#        the_date = date(2018, 9, 25) # use date.today() here#

#        midnight_without_tzinfo = datetime.combine(the_date, time())
#        print (midnight_without_tzinfo)
#        #2012-04-01 00:00:00#

#        midnight_with_tzinfo = tz.localize(midnight_without_tzinfo) 
#        print  (midnight_with_tzinfo)
#        #2012-04-01 00:00:00+11:00#

#        print  (midnight_with_tzinfo.astimezone(pytz.utc))
#        #2012-03-31 13:00:00+00:00


#        event = service.events().insert(calendarId='primary', body={
#            'summary': 'hola',
#            'description': 'chao',
#            #'start': {'dateTime': start_datetime.isoformat()},
#            #'end': {'dateTime': (start_datetime + timedelta(days=8)).isoformat()},
#            'start': {'dateTime': midnight_with_tzinfo},
#            'end': {'dateTime': datetime_object2.isoformat()},
#        }).execute()
#        print(event)

#        events_result = service.events().list(calendarId='primary', 
#                                       ).execute()
#        events = events_result.get('items', [])#

#        e = []#

#        if not events:
#            print('No upcoming events found.')
#        for event in events:
#            start = event['start'].get('dateTime', event['start'].get('date'))
#            end = event['end'].get('dateTime', event['end'].get('date'))#

#            e.append({ 'title':  event['summary'], 'start': start, 'end': end})



    